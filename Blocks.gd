extends TileMap

const PLACE_FORCE = 16
const MARGIN_TILES = 2

var _vel = Vector2()
var _accel = Vector2()
var _placecell = Vector2()
var _started = false
var _numblocks = 0
var _numplaced = 0
var _maxblocks = 0
var _time = 0

signal game_started
signal num_blocks
signal game_lost

func _ready():
	_numblocks = len(get_used_cells())
	_maxblocks = max(_maxblocks, _numblocks)
	emit_signal("num_blocks", _numblocks)

func set_cursor(cell):
	_placecell = cell
	$CursorAnimator/CursorSprite.visible = _numblocks > 0 and get_cellv(cell) == INVALID_CELL
	$CursorAnimator/CursorSprite.position = transform.xform(map_to_world(cell))
	if can_place_block(cell):
		$CursorAnimator.play("block")
	else:
		$CursorAnimator.play("x")

func _process(delta):
	if _started:
		_time += delta
	var pos = transform.xform_inv(get_viewport().get_mouse_position())
	set_cursor(world_to_map(pos))

func get_bound_rect():
	var vprect = get_viewport_rect()
	return Rect2(world_to_map(transform.xform_inv(vprect.position))+Vector2(MARGIN_TILES, MARGIN_TILES), world_to_map(vprect.size)-Vector2(MARGIN_TILES*2, MARGIN_TILES*2))

func is_square_in_bounds(cell):
	var boundrect = get_bound_rect()
	if boundrect.position.x > cell.x or cell.x > boundrect.end.x:
		return false
	if boundrect.position.y > cell.y or cell.y > boundrect.end.y:
		return false
	return true

func can_place_block(cell):
	var empty = get_cellv(cell) == INVALID_CELL
	if not empty:
		return false
	var adjacent = get_cell(cell.x - 1, cell.y) != INVALID_CELL
	adjacent = adjacent or get_cell(cell.x + 1, cell.y) != INVALID_CELL
	adjacent = adjacent or get_cell(cell.x, cell.y - 1) != INVALID_CELL
	adjacent = adjacent or get_cell(cell.x, cell.y + 1) != INVALID_CELL
	if not adjacent:
		return false
	return is_square_in_bounds(cell)

func _physics_process(delta):
	update_physics(delta)
	break_out_of_bound_blocks()

func update_physics(delta):
	_vel += _accel*delta
	position += _vel*delta

func break_out_of_bound_blocks():
	var usedrect = get_used_rect()
	var boundrect = get_bound_rect()
	for r in range(usedrect.position.y, usedrect.end.y):
		for c in range(usedrect.position.x, boundrect.position.x) + range(boundrect.end.x+1, usedrect.end.x):
			break_block(c, r)
	for c in range(usedrect.position.x, usedrect.end.x):
		for r in range(usedrect.position.y, boundrect.position.y) + range(boundrect.end.y+1, usedrect.end.y):
			break_block(c, r)

func break_block(c, r):
	if get_cell(c, r) != INVALID_CELL:
		_numblocks -= 1
		emit_signal("num_blocks", _numblocks)
		if _numblocks == 0:
			emit_signal("game_lost", _numplaced, _maxblocks)
	set_cell(c, r, INVALID_CELL)

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			place_block(_placecell)

func place_block(cell):
	if not cell or not can_place_block(cell):
		return
	set_cellv(cell, randi() % 6)
	_numblocks += 1
	_numplaced += 1
	_maxblocks = max(_maxblocks, _numblocks)
	emit_signal("num_blocks", _numblocks)
	var placeforce = _time
	if get_cell(cell.x - 1, cell.y) != INVALID_CELL:
		_accel += Vector2.RIGHT * placeforce
	if get_cell(cell.x + 1, cell.y) != INVALID_CELL:
		_accel += Vector2.LEFT * placeforce
	if get_cell(cell.x, cell.y - 1) != INVALID_CELL:
		_accel += Vector2.DOWN * placeforce
	if get_cell(cell.x, cell.y + 1) != INVALID_CELL:
		_accel += Vector2.UP * placeforce
	if not _started:
		_started = true
		emit_signal("game_started")
