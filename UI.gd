extends Control

var _time = 0
var _started = false

func _ready():
	$GameOver.visible = false

func _process(delta):
	if _started:
		_time += delta
	var msecs = int(_time * 1000) % 1000
	var secs = int(_time) % 60
	var mins = floor(_time / 60)
	$Clock.text = "%d:%02d.%03d" % [mins, secs, msecs]

func _on_game_started():
	_started = true
	$Instruction.visible = false

func _on_game_lost(numplaced, maxblocks):
	_started = false
	$GameOver.visible = true
	var text = $GameOver/Result.text
	$GameOver/Result.text = text % [numplaced, maxblocks]

func _on_num_blocks(numblocks):
	$NumBlocks.text = str(numblocks)
