extends Node2D

func _unhandled_key_input(event):
	if event.scancode == KEY_R:
		get_tree().reload_current_scene()
	elif event.scancode == KEY_ESCAPE:
		get_tree().quit()